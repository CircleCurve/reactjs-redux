import React from 'react';
import { User } from '../components/User';
import { Main } from '../components/Main';
import { connect } from 'react-redux';

import {setName, setAge} from '../actions/userAction'; 
class App extends React.Component {

    render() {
        return (
            <div className="container">
                <Main changeUserName={() => this.props.setName("Annie")} />
                <User username={this.props.user.name} />

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        math: state.math
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setName: (name) => {
            dispatch(setName(name));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App); 