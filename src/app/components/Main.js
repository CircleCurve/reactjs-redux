import React from 'react';
//stateless component
export const Main = (props) => {
    return (
        <div>
            <div className="row">
                <div className="col-xs-12">
                    <h1>The Main Page</h1>
                </div>
            </div>

            <div className="row">
                <div className="col-xs-12">
                    <button
                        className="btn btn-primary"
                        onClick={() => props.changeUserName('Anna')}
                    >
                        Change the user name
                        </button>

                </div>
            </div>

        </div>
    );
}
